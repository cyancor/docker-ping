FROM ubuntu:latest
LABEL maintainer="CyanCor GmbH - https://cyancor.com/"

RUN apt-get update \
    && DEBIAN_FRONTEND=noninteractive apt-get install --yes \
        sudo \
        curl \
        wget \
        inetutils-ping \
# Finalize
    && rm -rf /var/lib/apt/lists/*
